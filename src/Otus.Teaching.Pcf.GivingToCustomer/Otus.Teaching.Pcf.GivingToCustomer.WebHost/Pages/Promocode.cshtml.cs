using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class PromocodeModel : PageModel
    {
        public bool IsCreate => PromoCodeViewModel.Id.Equals(default);

        public string Title => (IsCreate ? "Create" : "Update") + " promocode";

        public PromoCodeViewModel PromoCodeViewModel { get; set; } = new PromoCodeViewModel();

        public IEnumerable<PreferenceForPromoCodeViewModel> AllPreferenceViewModels { get; set; } = new List<PreferenceForPromoCodeViewModel>();

        private readonly IRepository<PromoCode> _promocodeRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        public const string returnPage = "PromocodeList";


        public PromocodeModel(IRepository<PromoCode> promocodeRepository,
            IRepository<Preference> preferenceRepository) {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task OnGetCreate() {
            PromoCodeViewModel = new PromoCodeViewModel();
            AllPreferenceViewModels = (await _preferenceRepository.GetAllAsync()).Select(x => x.Adapt<PreferenceForPromoCodeViewModel>());
        }

        public async Task OnGetUpdate(Guid id) {
            var promocodeEntity = await _promocodeRepository.GetByIdAsync(id);
            PromoCodeViewModel = promocodeEntity.Adapt<PromoCodeViewModel>();

            AllPreferenceViewModels = (await _preferenceRepository.GetAllAsync()).Select(x => {
                var pref = x.Adapt<PreferenceForPromoCodeViewModel>();
                pref.IsSelected = promocodeEntity.PreferenceId == x.Id;
                return pref;
            });
        }

        public async Task<ActionResult> OnPostCreate(PromoCodeViewModel promoCodeViewModel) {
            await _promocodeRepository.AddAsync(promoCodeViewModel.Adapt<PromoCode>());
            return RedirectToPage(returnPage);
        }

        public async Task<ActionResult> OnPostUpdate(PromoCodeViewModel promoCodeViewModel) {
            await _promocodeRepository.UpdateAsync(promoCodeViewModel.Adapt<PromoCode>());
            return RedirectToPage(returnPage);
        }

        public async Task<ActionResult> OnPostDelete(Guid id) {
            await _promocodeRepository.DeleteAsync(id);
            return RedirectToPage(returnPage);
        }
    }
}