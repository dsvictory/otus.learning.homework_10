using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class PreferenceListModel : PageModel {

        private readonly IRepository<Preference> _preferenceRepository;

        public IEnumerable<PreferenceViewModel> PreferenceViewModels { get; set; }

        public PreferenceListModel(IRepository<Preference> preferenceRepository) {
            _preferenceRepository = preferenceRepository;
        }

        public async Task OnGetAsync() {
            var preferenceEntities = await _preferenceRepository.GetAllAsync();
            PreferenceViewModels = preferenceEntities?.Select(x => x.Adapt<PreferenceViewModel>()) ?? new List<PreferenceViewModel>();
        }
    }
}
