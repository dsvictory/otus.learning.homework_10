﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions {
    public interface IAppliedPromocodesUpdater {

        Task UpdateAppliedPromocodesAsync(Guid employeeId);

    }
}
