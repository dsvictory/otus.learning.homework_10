﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Mappers;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Logic {
    public class PromoCodesToCustomerGiver : IPromoCodesToCustomerGiver {

        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromoCodesToCustomerGiver(
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository,
            IRepository<PromoCode> promoCodesRepository) 
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeCommand command) {

                //Получаем предпочтение по имени
                var preference = await _preferencesRepository.GetByIdAsync(command.PreferenceId);

                if (preference == null) {
                    throw new KeyNotFoundException();
                }

                Console.WriteLine("Нашли предпочтения");

                //  Получаем клиентов с этим предпочтением:
                var customers = await _customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                        x.Preference.Id == preference.Id));

                Console.WriteLine("Нашли клиентов с ними");

                PromoCode promoCode = PromoCodeMapper.MapFromModel(command, preference, customers);

                Console.WriteLine("Смапили промокод");

                await _promoCodesRepository.AddAsync(promoCode);

                Console.WriteLine("Сохранили промокод");

                var test = await _promoCodesRepository.GetAllAsync();

                Console.WriteLine("Типа тестово вытащили промокоды");

        }
    }
}