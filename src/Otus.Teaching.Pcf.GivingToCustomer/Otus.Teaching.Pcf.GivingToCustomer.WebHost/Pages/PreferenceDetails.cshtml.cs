using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class PreferenceDetailsModel : PageModel
    {
        public PreferenceViewModel PreferenceViewModel { get; set; }

        private readonly IRepository<Preference> _preferenceRepository;

        public const string returnUrl = "PreferenceList";

        public PreferenceDetailsModel(IRepository<Preference> preferenceRepository) {
            _preferenceRepository = preferenceRepository;
        }

        public async Task OnGet(Guid id) {
            PreferenceViewModel = (await _preferenceRepository.GetByIdAsync(id)).Adapt<PreferenceViewModel>();
        }
    }
}