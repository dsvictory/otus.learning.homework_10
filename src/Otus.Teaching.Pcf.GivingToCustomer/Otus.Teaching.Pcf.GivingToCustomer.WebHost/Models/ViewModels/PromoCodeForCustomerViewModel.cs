﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels {
    public class PromoCodeForCustomerViewModel {

        public Guid Id { get; set; }

        public string Code { get; set; }

        public bool IsSelected { get; set; }

    }
}
