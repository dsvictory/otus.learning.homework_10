using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class CustomerDetailsModel : PageModel
    {
        public Customer Customer { get; set; } = new Customer();

        public IEnumerable<Preference> Preferences { get; set; } = new List<Preference>();

        public IEnumerable<PromoCode> Promocodes { get; set; } = new List<PromoCode>();

        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        private readonly IRepository<PromoCode> _promocodeRepository;

        public CustomerDetailsModel(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promocodeRepository
            ) 
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
        }

        public async Task OnGet(Guid id) {
            Customer = await _customerRepository.GetByIdAsync(id);

            if (Customer.Preferences != null) {
                var preferenceIds = Customer.Preferences.Select(x => x.PreferenceId).ToList();
                Preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);
            }

            if (Customer.PromoCodes != null) {
                var promocodeIds = Customer.PromoCodes.Select(x => x.PromoCodeId).ToList();
                Promocodes = await _promocodeRepository.GetRangeByIdsAsync(promocodeIds);
            } 
        }
    }
}