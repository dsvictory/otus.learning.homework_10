﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Logic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions {
    public interface IPromoCodesToCustomerGiver {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeCommand command);
    }
}
