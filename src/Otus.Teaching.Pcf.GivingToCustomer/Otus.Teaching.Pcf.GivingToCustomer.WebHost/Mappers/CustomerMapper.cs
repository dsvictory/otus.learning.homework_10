﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }

        public static Customer MapFromViewModel(CustomerViewModel model) {

            var customer = model.Adapt<Customer>();

            customer.Preferences = model.PreferenceIds?.Select(x => new CustomerPreference() {
                CustomerId = model.Id,
                PreferenceId = x
            })
            .ToList();

            customer.PromoCodes = model.PromocodeIds?.Select(x => new PromoCodeCustomer() {
                CustomerId = model.Id,
                PromoCodeId = x
            })
            .ToList();

            return customer;
        }
    }
}