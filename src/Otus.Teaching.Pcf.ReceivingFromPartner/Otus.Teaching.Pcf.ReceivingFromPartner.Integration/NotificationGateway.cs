﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class NotificationGateway
        : INotificationGateway
    {
        private const string EXCHANGE_NAME = "partner-manager-promocode";

        public Task SendNotificationToPartnerAsync(Guid partnerId, string message)
        {
            //Код, который вызывает сервис отправки уведомлений партнеру
            
            return Task.CompletedTask;
        }

        public Task SendNotificationAboutPartnerManagerPromoCode(PromoCode promoCode) {

            return Task.Run(() => {

                var factory = new ConnectionFactory() {
                    HostName = "localhost"
                };

                using var connection = factory.CreateConnection();

                using var channel = connection.CreateModel();

                channel.ExchangeDeclare(
                    exchange: EXCHANGE_NAME, 
                    type: ExchangeType.Fanout);

                var dto = new GivePromoCodeToCustomerDto()
                {
                    PartnerId = promoCode.Partner.Id,
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString(),
                    PreferenceId = promoCode.PreferenceId,
                    PromoCode = promoCode.Code,
                    ServiceInfo = promoCode.ServiceInfo,
                    PartnerManagerId = promoCode.PartnerManagerId
                };

                var json = JsonSerializer.Serialize(dto);

                var message = Encoding.UTF8.GetBytes(json);

                channel.BasicPublish(
                    exchange: "partner-manager-promocode",
                    routingKey: "",
                    basicProperties: null,
                    body: message
                );

                Console.WriteLine($"Message {json} was sent");

            });

        }
    }
}