using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class CustomerModel : PageModel
    {
        public bool IsCreate => CustomerViewModel.Id.Equals(default);

        public string Title => (IsCreate ? "Create" : "Update") + " customer";

        public CustomerViewModel CustomerViewModel { get; set; } = new CustomerViewModel();

        public IEnumerable<PreferenceForCustomerViewModel> AllPreferenceViewModels { get; set; } = new List<PreferenceForCustomerViewModel>();

        public IEnumerable<PromoCodeForCustomerViewModel> AllPromocodeViewModels { get; set; } = new List<PromoCodeForCustomerViewModel>();

        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        private readonly IRepository<PromoCode> _promocodeRepository;

        public const string returnPage = "CustomerList";

        public CustomerModel(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promocodeRepository) 
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
        }

        public async Task OnGetCreate() {
            AllPreferenceViewModels = (await _preferenceRepository.GetAllAsync()).Select(x => x.Adapt<PreferenceForCustomerViewModel>());
            AllPromocodeViewModels = (await _promocodeRepository.GetAllAsync()).Select(x => x.Adapt<PromoCodeForCustomerViewModel>());
        }

        public async Task OnGetUpdate(Guid id) {
            var customeEntity =  await _customerRepository.GetByIdAsync(id);
            CustomerViewModel = customeEntity.Adapt<CustomerViewModel>();

            if (CustomerViewModel.Preferences != null) {
                var preferenceIds = customeEntity.Preferences.Select(x => x.PreferenceId).ToList();
                AllPreferenceViewModels = (await _preferenceRepository.GetAllAsync()).Select(x => {
                    var pref = x.Adapt<PreferenceForCustomerViewModel>();
                    pref.IsSelected = preferenceIds.Contains(x.Id);
                    return pref;
                });
            }
            else {
                AllPreferenceViewModels = (await _preferenceRepository.GetAllAsync()).Select(x => x.Adapt<PreferenceForCustomerViewModel>());
            }

            if (CustomerViewModel.PromoCodes != null) {
                var promocodeIds = customeEntity.PromoCodes.Select(x => x.PromoCodeId).ToList();
                AllPromocodeViewModels = (await _promocodeRepository.GetAllAsync()).Select(x => {
                    var promocode = x.Adapt<PromoCodeForCustomerViewModel>();
                    promocode.IsSelected = promocodeIds.Contains(x.Id);
                    return promocode;
                });
            }
            else {
                AllPromocodeViewModels = (await _promocodeRepository.GetAllAsync()).Select(x => x.Adapt<PromoCodeForCustomerViewModel>());
            }
        }

        public async Task<ActionResult> OnPostCreate(CustomerViewModel customerViewModel) {

            var customerEntity = CustomerMapper.MapFromViewModel(customerViewModel);
            await _customerRepository.AddAsync(customerEntity);
            return RedirectToPage(returnPage);
        }

        public async Task<ActionResult> OnPostUpdate(CustomerViewModel customerViewModel) {
            var customerEntity = CustomerMapper.MapFromViewModel(customerViewModel);
            await _customerRepository.UpdateAsync(customerEntity);
            return RedirectToPage(returnPage);
        }

        public async Task<ActionResult> OnPostDelete(Guid id) {
            await _customerRepository.DeleteAsync(id);
            return RedirectToPage(returnPage);
        }
    }


}
