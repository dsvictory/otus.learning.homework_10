﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels {
    public class CustomerViewModel {

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public IEnumerable<Guid> PreferenceIds { get; set; }

        public IEnumerable<PreferenceViewModel> Preferences { get; set; }

        public IEnumerable<Guid> PromocodeIds { get; set; }

        public IEnumerable<PromoCodeViewModel> PromoCodes { get; set; }
    }
}
