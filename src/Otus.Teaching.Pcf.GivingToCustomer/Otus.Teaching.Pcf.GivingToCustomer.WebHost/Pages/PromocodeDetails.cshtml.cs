using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class PromocodeDetailsModel : PageModel
    {
        public PromoCodeViewModel PromoCodeViewModel { get; set; }

        private readonly IRepository<PromoCode> _promocodeRepository;

        public const string returnPage = "PromocodeList";

        public PromocodeDetailsModel(IRepository<PromoCode> promocodeRepository) {
            _promocodeRepository = promocodeRepository;
        }

        public async Task OnGet(Guid id) {
            var promocodeEntity = await _promocodeRepository.GetByIdAsync(id);
            PromoCodeViewModel = promocodeEntity.Adapt<PromoCodeViewModel>();
            PromoCodeViewModel.PreferenceName = promocodeEntity.Preference.Name;

            if (PromoCodeViewModel.Customers == null) {
                PromoCodeViewModel.Customers = new List<CustomerViewModel>();
            }
        }
    }
}