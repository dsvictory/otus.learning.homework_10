﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administation.Integration.Dto;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Logic;

namespace Otus.Teaching.Pcf.Administation.Integration {
    public class NotificationReceiverService : BackgroundService {

        private readonly ConnectionFactory _factory;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        private readonly IServiceScopeFactory _scopeFactory;

        private const string NOTIFICATION_QUEUE_NAME = "promocode-apllied-administration";
        private const string EXCHANGE_NAME = "partner-manager-promocode";

        public NotificationReceiverService(IServiceScopeFactory scopeFactory) {

            _scopeFactory = scopeFactory;

            _factory = new ConnectionFactory() {
                HostName = "localhost"
            };

            _connection = _factory.CreateConnection();

            _channel = _connection.CreateModel();

            _channel.QueueDeclare(
                queue: NOTIFICATION_QUEUE_NAME,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );

            _channel.ExchangeDeclare(
                exchange: EXCHANGE_NAME,
                type: ExchangeType.Fanout);

            _channel.QueueBind(
                queue: NOTIFICATION_QUEUE_NAME,
                exchange: EXCHANGE_NAME,
                routingKey: "");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {

            /*
             * Задержка добавлена, чтобы запись промокодов в БД не началась
             * до окончания инициализации и сидирования самой БД
             */
            await Task.Delay(10000);

            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += async (model, ea) =>
            {
                var body = ea.Body.ToArray();

                var json = Encoding.UTF8.GetString(body);

                var message = JsonSerializer.Deserialize<GivePromoCodeToCustomerReceivedMessage>(json);

                Console.WriteLine($"Received message: {json}");

                if (message.PartnerManagerId == null) return;
                var partnerManagerId = message.PartnerManagerId.Value;

                using (var scope = _scopeFactory.CreateScope())
                {
                    var appliedPromocodesUpdater = scope.ServiceProvider.GetRequiredService<IAppliedPromocodesUpdater>();

                    await appliedPromocodesUpdater.UpdateAppliedPromocodesAsync(partnerManagerId);
                }
            };

            _channel.BasicConsume(queue: NOTIFICATION_QUEUE_NAME, autoAck: true, consumer: consumer);
        }

    }
}
