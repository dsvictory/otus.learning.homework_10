using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class PromocodeListModel : PageModel
    {
        private readonly IRepository<PromoCode> _promocodeRepository;

        public IEnumerable<PromoCodeViewModel> PromocodeViewModels { get; set; }

        public PromocodeListModel(IRepository<PromoCode> promocodeRepository) {
            _promocodeRepository = promocodeRepository;
        }

        public async Task OnGetAsync() {
            var promocodeEntities = await _promocodeRepository.GetAllAsync();
            PromocodeViewModels = promocodeEntities?.Select(x => x.Adapt<PromoCodeViewModel>()) ?? new List<PromoCodeViewModel>();
        }
    }
}
