﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels {
    public class PreferenceViewModel {

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
