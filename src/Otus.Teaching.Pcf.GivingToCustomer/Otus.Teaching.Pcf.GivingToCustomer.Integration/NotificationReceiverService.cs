﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto;
using Mapster;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Logic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration {
    public class NotificationReceiverService : BackgroundService {

        private readonly IConnection _connection;
        private readonly IModel _channel;

        private readonly IServiceScopeFactory _scopeFactory;

        private const string NOTIFICATION_QUEUE_NAME = "promocode-apllied-giving-to-customer";
        private const string EXCHANGE_NAME = "partner-manager-promocode";

        public NotificationReceiverService(IServiceScopeFactory scopeFactory) {

            _scopeFactory = scopeFactory;

            var factory = new ConnectionFactory() {
                HostName = "localhost"
            };

            _connection = factory.CreateConnection();

            _channel = _connection.CreateModel();

            _channel.QueueDeclare(
                queue: NOTIFICATION_QUEUE_NAME,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );

            _channel.ExchangeDeclare(
                exchange: EXCHANGE_NAME,
                type: ExchangeType.Fanout);

            _channel.QueueBind(
                queue: NOTIFICATION_QUEUE_NAME,
                exchange: EXCHANGE_NAME,
                routingKey: "");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {

            /*
             * Задержка добавлена, чтобы запись промокодов в БД не началась
             * до окончания инициализации и сидирования самой БД
             */
            await Task.Delay(10000);

            if (stoppingToken.IsCancellationRequested) {
                _channel.Dispose();
                _connection.Dispose();
                return;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += async (model, ea) => {
                var body = ea.Body.ToArray();

                var json = Encoding.UTF8.GetString(body);

                var message = JsonSerializer.Deserialize<GivePromoCodeToCustomerReceivedMessage>(json);

                Console.WriteLine($"Received message: {json}");

                /*
                 * Объект для проведения тестов без внешних микросервисов
                 */
                //var test = new GivePromoCodeCommand() {
                //    BeginDate = "02.10.2021",
                //    EndDate = "01.11.2021",
                //    PartnerId = Guid.Parse("20d2d612-db93-4ed5-86b1-ff2413bca655"),
                //    PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                //    PromoCode = "H123124",
                //    ServiceInfo = "Билеты на лучший спектакль сезона"
                //};

                using (var scope = _scopeFactory.CreateScope()) {
                    var promoCodesToCustomerGiver = scope.ServiceProvider.GetRequiredService<IPromoCodesToCustomerGiver>();

                    await promoCodesToCustomerGiver.GivePromoCodesToCustomersWithPreferenceAsync(message.Adapt<GivePromoCodeCommand>());
                }

            };

            _channel.BasicConsume(
                queue: NOTIFICATION_QUEUE_NAME, 
                autoAck: true, 
                consumer: consumer);
        }

    }
}
