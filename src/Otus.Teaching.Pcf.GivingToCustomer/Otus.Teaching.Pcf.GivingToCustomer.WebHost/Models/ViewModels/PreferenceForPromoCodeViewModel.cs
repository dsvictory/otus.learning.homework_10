﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels {
    public class PreferenceForPromoCodeViewModel {

        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }

    }
}
