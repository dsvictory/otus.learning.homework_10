using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class CustomerListModel : PageModel {

        private readonly IRepository<Customer> _customerRepository;

        public IEnumerable<Customer> Customers { get; set; }

        public CustomerListModel(IRepository<Customer> customerRepository) {
            _customerRepository = customerRepository;
        }

        public async Task OnGetAsync() {
            Customers = await _customerRepository.GetAllAsync();
        }
    }
}
