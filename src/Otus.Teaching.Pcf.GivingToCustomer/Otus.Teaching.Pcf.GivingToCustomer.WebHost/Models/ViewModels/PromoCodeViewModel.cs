﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels {
    public class PromoCodeViewModel {

        public Guid Id { get; set; }

        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PreferenceId { get; set; }

        public string PreferenceName { get; set; }

        public virtual ICollection<CustomerViewModel> Customers { get; set; }

    }
}
