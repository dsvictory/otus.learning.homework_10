using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.ViewModels;
using Mapster;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Pages
{
    public class PreferenceModel : PageModel
    {
        public bool IsCreate => PreferenceViewModel.Id.Equals(default);

        public string Title => (IsCreate ? "Create" : "Update") + " preference";

        public PreferenceViewModel PreferenceViewModel { get; set; }

        private readonly IRepository<Preference> _preferenceRepository;

        public const string returnPage = "PreferenceList";


        public PreferenceModel(IRepository<Preference> preferenceRepository) {
            _preferenceRepository = preferenceRepository;  
        }

        public void OnGetCreate() {
            PreferenceViewModel = new PreferenceViewModel();
        }

        public async Task OnGetUpdate(Guid id) {
            var preferenceEntity = await _preferenceRepository.GetByIdAsync(id);
            PreferenceViewModel = preferenceEntity.Adapt<PreferenceViewModel>();
        }

        public async Task<ActionResult> OnPostCreate(PreferenceViewModel preferenceViewModel) {
            await _preferenceRepository.AddAsync(preferenceViewModel.Adapt<Preference>());
            return RedirectToPage(returnPage);
        }

        public async Task<ActionResult> OnPostUpdate(PreferenceViewModel preferenceViewModel) {
            await _preferenceRepository.UpdateAsync(preferenceViewModel.Adapt<Preference>());
            return RedirectToPage(returnPage);
        }

        public async Task<ActionResult> OnPostDelete(Guid id) {
            await _preferenceRepository.DeleteAsync(id);
            return RedirectToPage(returnPage);
        }
    }
}
